Bloc 3 - Architectures matérielles et robotique, systèmes et réseaux
====================================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Intervenants - contacts
=======================

Benoit Papegay,
Jean-François Roos,
Jean-Luc Levaire,
Laurent Noé,
Philippe Marquet,
Yvan Peter

Et
Gilles Grimaud

Mais aussi
Pierre Boulet,
Thomas Vantroys,
Yann Secq

Prise en main de l'environnement
================================

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Très et trop brève introduction
  - Découverte de l'interpréteur de commandes
* [GitLab](seance0/gitlab.md)
  - Découverte de l'environnement GitLab
* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Utiliser l'interpréteur de commandes
  - ...

Architecture des machines informatiques et des systèmes d'exploitation
======================================================================

conférence de Gilles Grimaud, _Turing, von Neumann, ... architecture des machines
informatiques et des systèmes d'exploitation_

* Machine de Turing
  [page Wikipedia fr](https://fr.wikipedia.org/wiki/Machine_de_Turing)
* article fondateur d'Alan Turing, 1936 _« On Computable Numbers, with
  an Application to the Entscheidungsproblem »_
 - notes 10 et 11 de la page Wikipedia [Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing#cite_note-on_computable_numbers-10) 
* _Le modèle d’architecture de von Neumann_ par Sacha Krakowiak, sur
  [interstices.info/](https://interstices.info/le-modele-darchitecture-de-von-neumann/)


Machine virtuelle
=================

* [Installation de VirtualBox](virtualbox/seance-virtualisation.md)

Interpréteur de commandes
=========================

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - (suite...)


Ressources
==========

Carte de référence Unix de Moïse Valvassori

* [unix-refcard.pdf sur www.ai.univ-paris8.fr](http://www.ai.univ-paris8.fr/~djedi/poo/unix-refcard.pdf)
* [copie locale](doc/unix-refcard.pdf)

